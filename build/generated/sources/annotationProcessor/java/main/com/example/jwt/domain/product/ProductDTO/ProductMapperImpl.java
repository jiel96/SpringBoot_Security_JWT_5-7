package com.example.jwt.domain.product.ProductDTO;

import com.example.jwt.domain.product.Product;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-02T14:24:04+0100",
    comments = "version: 1.5.5.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.5.jar, environment: Java 17.0.9 (Eclipse Adoptium)"
)
@Component
public class ProductMapperImpl implements ProductMapper {

    @Override
    public Product fromDTO(ProductDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Product product = new Product();

        product.setId( dto.getId() );
        product.setName( dto.getName() );
        product.setPrice( dto.getPrice() );
        product.setCountry( dto.getCountry() );
        product.setQuantity( dto.getQuantity() );

        return product;
    }

    @Override
    public List<Product> fromDTOs(List<ProductDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        List<Product> list = new ArrayList<Product>( dtos.size() );
        for ( ProductDTO productDTO : dtos ) {
            list.add( fromDTO( productDTO ) );
        }

        return list;
    }

    @Override
    public Set<Product> fromDTOs(Set<ProductDTO> dtos) {
        if ( dtos == null ) {
            return null;
        }

        Set<Product> set = new LinkedHashSet<Product>( Math.max( (int) ( dtos.size() / .75f ) + 1, 16 ) );
        for ( ProductDTO productDTO : dtos ) {
            set.add( fromDTO( productDTO ) );
        }

        return set;
    }

    @Override
    public ProductDTO toDTO(Product BO) {
        if ( BO == null ) {
            return null;
        }

        String name = null;
        int price = 0;
        String country = null;
        int quantity = 0;

        name = BO.getName();
        price = BO.getPrice();
        country = BO.getCountry();
        quantity = BO.getQuantity();

        ProductDTO productDTO = new ProductDTO( name, price, country, quantity );

        productDTO.setId( BO.getId() );

        return productDTO;
    }

    @Override
    public List<ProductDTO> toDTOs(List<Product> BOs) {
        if ( BOs == null ) {
            return null;
        }

        List<ProductDTO> list = new ArrayList<ProductDTO>( BOs.size() );
        for ( Product product : BOs ) {
            list.add( toDTO( product ) );
        }

        return list;
    }

    @Override
    public Set<ProductDTO> toDTOs(Set<Product> BOs) {
        if ( BOs == null ) {
            return null;
        }

        Set<ProductDTO> set = new LinkedHashSet<ProductDTO>( Math.max( (int) ( BOs.size() / .75f ) + 1, 16 ) );
        for ( Product product : BOs ) {
            set.add( toDTO( product ) );
        }

        return set;
    }
}
