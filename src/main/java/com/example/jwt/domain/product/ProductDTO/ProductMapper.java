package com.example.jwt.domain.product.ProductDTO;

import com.example.jwt.core.generic.ExtendedMapper;
import com.example.jwt.domain.product.Product;
import com.example.jwt.domain.user.User;
import com.example.jwt.domain.user.dto.UserRegisterDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProductMapper extends ExtendedMapper<Product, ProductDTO> {


}
