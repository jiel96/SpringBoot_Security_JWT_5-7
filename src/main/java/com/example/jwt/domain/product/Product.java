package com.example.jwt.domain.product;

import com.example.jwt.core.generic.ExtendedAuditEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "product")
public class Product extends ExtendedAuditEntity {

        @Column(name = "name", nullable = false)
        private String name;

        @Column(name = "price")
        private int price;
        @Column(name = "country")
        private String country;

        @Column(name = "quantity")
        private int quantity;

        public Product() {
        }

        public Product(UUID id, String name, int price, String country, int quantity) {
            super(id);
            this.name = name;
            this.price = price;
            this.country = country;
            this.quantity = quantity;
        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }
}
