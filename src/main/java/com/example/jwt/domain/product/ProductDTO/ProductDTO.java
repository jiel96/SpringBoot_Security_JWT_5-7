package com.example.jwt.domain.product.ProductDTO;

import com.example.jwt.core.generic.ExtendedDTO;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

public class ProductDTO extends ExtendedDTO {

    @NotNull
    private String name;
    @Max(100)
    private int price;

    private String country;

    private int quantity;

    public ProductDTO(String name, int price,String country,int quantity) {
        this.name = name;
        this.price = price;
        this.country = country;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public ProductDTO setName(String name) {
        this.name = name;
        return this;
    }

    public int getPrice() {
        return price;
    }

    public ProductDTO setPrice(int price) {
        this.price = price;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
